import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Subject, Observable } from "rxjs";
import { Sale } from "src/app/admin/shared/models/sale";

@Injectable()
export class SaleService {
  private subjectSaleList = new Subject<any>();
  private subjectSaleForm = new Subject<any>();

  constructor(public api: ApiService) {}

  createSale(sale: any) {
    sale.client_id = JSON.parse(localStorage.getItem("userLogged")).user_id;
    return this.api.post("api/sale/create-register", sale);
  }

  readSaleById(saleId: number) {
    return this.api.get(`api/sale/${saleId}`);
  }

  readSales() {
    var user = JSON.parse(localStorage.getItem("userLogged"));
    if (user.type_user == 0) {
      return this.api.get(`api/sale/list/${user.user_id}`);
    } else {
      return this.api.get(`api/sale/list/`);
    }
  }

  readSaleHistory() {
    var user = JSON.parse(localStorage.getItem("userLogged"));
    return this.api.get(`api/sale/history/by-user/${user.user_id}`);
  }

  readDetailSale(saleId) {
    return this.api.get(`api/sale/detail/${saleId}`);
  }

  createTransaction(registerId) {
    var body = {
      register_id: registerId,
      recycler_id: JSON.parse(localStorage.getItem("userLogged")).user_id,
    };
    return this.api.post(`api/sale/transaction`, body);
  }

  readSalesByUser() {
    return this.api.get("api/sale/all");
  }

  updateSale(sale: Sale) {
    return this.api.put("api/sale", sale);
  }

  deleteSale(saleId: number) {
    return this.api.delete(`api/sale/${saleId}`);
  }

  refreshSaleList(status: boolean) {
    this.subjectSaleList.next({ status });
  }

  listenerRefreshSaleList(): Observable<any> {
    return this.subjectSaleList.asObservable();
  }

  resetSaleForm() {
    this.subjectSaleForm.next(true);
  }

  listenerResetSaleForm(): Observable<any> {
    return this.subjectSaleForm.asObservable();
  }
}
