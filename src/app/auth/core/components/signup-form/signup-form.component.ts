import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "src/app/core/services/auth.service";
import { NotificationUtil } from "src/app/core/utils/notification.util";
import { Router } from "@angular/router";

@Component({
  selector: "signup-form",
  templateUrl: "./signup-form.component.html",
  styleUrls: ["./signup-form.component.scss"],
})
export class SignupFormComponent implements OnInit {
  public signupFG: FormGroup;
  public loading: boolean;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notification: NotificationUtil,
    private router: Router
  ) {}

  reset() {
    this.loading = false;
    this.signupFG = this.fb.group({
      name: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      email: ["", [Validators.email]],
      password: ["", [Validators.required]],
      type_user: ["", [Validators.required]],
    });
  }

  ngOnInit() {
    this.reset();
  }

  onSignup() {
    if (this.signupFG.valid) {
      const signupRequest = Object.assign({}, this.signupFG.value);
      this.loading = true;

      this.authService.signup(signupRequest).subscribe(
        (response: any) => {
          this.notification.success("", "Registro exitoso!");
          this.router.navigateByUrl("/auth/login");
          this.loading = false;
        },
        (error: any) => {
          this.loading = false;
          this.notification.error("", "Error");
        }
      );
    } else {
      this.notification.warning("", "Invalid form");
    }
  }
}
