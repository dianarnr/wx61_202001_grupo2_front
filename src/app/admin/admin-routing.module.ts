import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      {
        path: '', redirectTo: 'materials', pathMatch: 'full'
      },
      {
        path: 'materials',
        loadChildren:() => import('src/app/admin/materials/materials.module').then(m => m.MaterialsModule)
      },
      {
        path: 'sales',
        loadChildren:() => import('src/app/admin/sales/sales.module').then(m => m.SalesModule)
      },
      {
        path: 'history',
        loadChildren:() => import('src/app/admin/sales-history/sales-history.module').then(m => m.SalesHistoryModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
