import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Subscription, Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { MaterialService } from "src/app/core/services/material.service";
import { Material } from "src/app/admin/shared/models/material";
import { CRUD } from "src/app/core/constants/general.constant";

@Component({
  selector: "material-form",
  templateUrl: "./material-form.component.html",
  styleUrls: ["./material-form.component.scss"],
})
export class MaterialFormComponent implements OnInit {
  @Input() public method: string;

  public materialFG: FormGroup;
  public paramsHandler: Subscription;
  public materialId: number;
  public materialSubscription: Subscription;
  public loading: boolean;
  public categories: any[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private materialService: MaterialService
  ) {
    this.initRouteParamsListener();
    this.materialService.listenerRefreshMaterialList().subscribe((status) => {
      this.reset();
    });
  }

  reset() {
    this.materialFG = this.fb.group({
      product_id: [],
      material_id: ["", [Validators.required]],
      weight: ["", [Validators.required]],
      price_kg: ["", [Validators.required]],
    });
    this.loading = false;
  }

  ngOnInit() {
    this.reset();
    this.getCategories();
  }

  getCategories() {
    this.materialService.readCategories().subscribe(
      (response: any) => {
        this.categories = response;
      },
      (error: any) => {
        console.log("error", error);
      }
    );
  }

  getMaterial() {}

  initRouteParamsListener(): void {
    this.paramsHandler = this.route.queryParams.subscribe((params) => {
      this.materialId = params.materialId;
      if (this.materialId) this.getMaterial();
    });
  }

  onSubmit() {
    if (this.materialFG.valid) {
      let sale = Object.assign({}, this.materialFG.value);
      let request: Observable<any>;

      this.loading = true;
      if (this.method == CRUD.CREATE) {
        sale.client_id = JSON.parse(localStorage.getItem("userLogged")).user_id;
        sale.total_product = sale.price_kg * sale.weight;
        request = this.materialService.createMaterial(sale);
      } else {
        request = this.materialService.updateMaterial(sale);
      }

      request.subscribe(
        (response: any) => {
          if (this.method == CRUD.CREATE)
            this.materialService.refreshMaterialList(true);
          if (this.method == CRUD.UPDATE) this.router.navigate(["/material"]);
          this.loading = false;
        },
        (error: any) => {
          this.loading = false;
        }
      );
    } else {
      console.log("invalid form");
    }
  }

  cancelRequest() {
    if (this.method == CRUD.CREATE) {
      this.materialService.refreshMaterialList(false);
    } else {
      this.router.navigate(["/material"]);
    }
  }
}
