import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSaleViewComponent } from './new-sale-view.component';

describe('NewSaleViewComponent', () => {
  let component: NewSaleViewComponent;
  let fixture: ComponentFixture<NewSaleViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewSaleViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSaleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
