import { Component, OnInit } from '@angular/core';
import { CRUD } from 'src/app/core/constants/general.constant';

@Component({
  selector: 'app-new-sale-view',
  templateUrl: './new-sale-view.component.html',
  styleUrls: ['./new-sale-view.component.scss']
})
export class NewSaleViewComponent implements OnInit {
  public method = CRUD;

  constructor() { }

  ngOnInit(): void {
  }

}
