import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-sale-list-view",
  templateUrl: "./sale-list-view.component.html",
  styleUrls: ["./sale-list-view.component.scss"],
})
export class SaleListViewComponent implements OnInit {
  public typeUser: any;

  constructor() {}

  ngOnInit() {
    this.typeUser = JSON.parse(localStorage.getItem("userLogged")).type_user;
  }
}
