import { Component, OnInit } from "@angular/core";
import { Sale } from "src/app/admin/shared/models/sale";
import { CRUD } from "src/app/core/constants/general.constant";
import { Subscription } from "rxjs";
import { SaleService } from "src/app/core/services/sale.service";
import { Router } from "@angular/router";
import { NotificationUtil } from "src/app/core/utils/notification.util";

@Component({
  selector: "sale-table",
  templateUrl: "./sale-table.component.html",
  styleUrls: ["./sale-table.component.scss"],
})
export class SaleTableComponent implements OnInit {
  public sales: Sale[];
  public isVisible: boolean = false;
  public method = CRUD;
  public saleSubscription: Subscription;
  public loading: boolean;

  public materials = [];
  public buyVisible = false;
  public elementSelected: any;

  public typeUser: any;

  constructor(
    private saleService: SaleService,
    private router: Router,
    private notification: NotificationUtil
  ) {}

  reset() {
    this.typeUser = JSON.parse(localStorage.getItem("userLogged")).type_user;
    this.sales = [];
    this.loading = false;
  }

  getSales() {
    this.loading = true;
    this.saleService.readSales().subscribe(
      (response: any) => {
        this.loading = false;
        this.sales = response;
      },
      (error: any) => {
        this.loading = false;
      }
    );
  }

  ngOnInit() {
    this.reset();
    this.getSales();
  }

  showDetail(saleId): void {
    this.isVisible = true;
    this.saleService.readDetailSale(saleId).subscribe(
      (response: any) => {
        this.materials = response;
      },
      (error: any) => {
        this.loading = false;
      }
    );
  }

  hideModal(): void {
    this.isVisible = false;
    this.materials = [];
  }

  edit(saleId: number) {
    this.router.navigate(["/sales/edit"], { queryParams: { saleId } });
  }

  delete(saleId: number) {
    /* this.saleService.deleteSale(saleId).subscribe(
      (response: any)=>{
        this.getSales();
      },
      (error: any)=>{
        console.log('error', error);
      }
    ) */
  }

  showBuyModal(id): void {
    this.elementSelected = id;
    this.buyVisible = true;
  }

  hideBuyModal(): void {
    this.buyVisible = false;
    this.elementSelected = null;
  }

  buyThings() {
    this.saleService.createTransaction(this.elementSelected).subscribe(
      (response: any) => {
        this.router.navigateByUrl("/history");
        this.notification.success("Registro exitoso", "");
      },
      (error: any) => {
        this.loading = false;
      }
    );
  }
}
