import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { Subscription, Observable } from "rxjs";
import { SaleService } from "src/app/core/services/sale.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Sale } from "src/app/admin/shared/models/sale";
import { CRUD } from "src/app/core/constants/general.constant";
import { MaterialService } from "src/app/core/services/material.service";

@Component({
  selector: "sale-form",
  templateUrl: "./sale-form.component.html",
  styleUrls: ["./sale-form.component.scss"],
})
export class SaleFormComponent implements OnInit {
  @Input() public method: string;

  public saleFG: FormGroup;
  public saleId: number;
  public loading: boolean;
  public materials = [];

  public paramsHandler: Subscription;
  public saleSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private saleService: SaleService,
    private route: ActivatedRoute,
    private router: Router,
    private materialService: MaterialService
  ) {
    this.initRouteParamsListener();
  }

  reset() {
    this.saleFG = this.fb.group({
      register_id: [],
      sold_or_given: ["0", [Validators.required]],
      materials: this.fb.array([]),
    });
    this.materials = [];
    this.loading = false;
  }

  getMaterials() {
    this.materialService.readMaterials().subscribe(
      (response: any) => {
        this.materials = response.filter((p) => p.register_id == 0);
      },
      (error: any) => {
        console.log("error", error);
      }
    );
  }

  ngOnInit() {
    this.reset();
    this.getMaterials();
    this.addField();
  }

  getSale() {
    this.saleService.readSaleById(this.saleId).subscribe(
      (response: any) => {
        let quantityVideos = response.data.videos.length - 1;
        while (quantityVideos > 0) {
          this.addField();
          quantityVideos--;
        }

        var arrMaterial = [];
        for (let material of response.materials) {
          arrMaterial.push(material);
        }
        response.materials = arrMaterial;

        this.saleFG.patchValue(response);
      },
      (error: any) => {
        console.log("error", error);
      }
    );
  }

  initRouteParamsListener(): void {
    this.paramsHandler = this.route.queryParams.subscribe((params) => {
      this.saleId = params.saleId;
      if (this.saleId) this.getSale();
    });
  }

  onSubmit() {
    if (this.saleFG.valid) {
      let sale = Object.assign({}, this.saleFG.value);
      let request: Observable<any>;

      sale.total_amount = 0;
      for (let item of sale.materials) {
        var product = this.materials.filter(
          (p) => p.product_id == item.product_id
        );
        item.total_product = product[0].weight * product[0].price_kg;
        sale.total_amount = +item.total_product;
      }
      console.log("request", sale);

      this.loading = true;
      if (this.method == CRUD.CREATE) {
        request = this.saleService.createSale(sale);
      } else {
        request = this.saleService.updateSale(sale);
      }

      request.subscribe(
        (response: any) => {
          this.router.navigate(["/sales"]);
          this.loading = false;
        },
        (error: any) => {
          this.loading = false;
        }
      );
    } else {
      console.log("invalid form");
    }
  }

  cancelRequest() {
    this.router.navigate(["/sales"]);
  }

  addField(e?: MouseEvent): void {
    if (e) e.preventDefault();
    const materials = this.saleFG.controls.materials as FormArray;
    materials.push(
      this.fb.group({
        product_id: [0, [Validators.required]],
        weight: [0, [Validators.required]],
      })
    );
  }

  removeField(i: any, e: MouseEvent): void {
    e.preventDefault();
    const materials = this.saleFG.controls.materials as FormArray;
    if (this.saleFG.value.materials.length != 1) materials.removeAt(i);
  }
}
