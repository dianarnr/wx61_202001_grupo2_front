import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesHistoryViewComponent } from './sales-history-view/sales-history-view.component';

const routes: Routes = [
  {
    path: '',
    component: SalesHistoryViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule { }
