import { Component, OnInit } from "@angular/core";
import { SaleService } from "src/app/core/services/sale.service";

@Component({
  selector: "app-sales-history-view",
  templateUrl: "./sales-history-view.component.html",
  styleUrls: ["./sales-history-view.component.scss"],
})
export class SalesHistoryViewComponent implements OnInit {
  public sales: any[];
  public loading: boolean;
  public isVisible: boolean = false;
  public materials: any[];

  constructor(private saleService: SaleService) {}

  reset() {
    this.sales = [];
    this.loading = false;
  }

  getSales() {
    this.loading = true;
    this.sales = [
      {
        id: 1,
        date: "2020/02/2020",
        type: "Regalo",
        weight: 30,
        total: 8.3,
        status: "Aceptado",
        detail: [
          {
            type: "plastico",
            weight: "20",
            price: "122.2",
            total: "232.0",
          },
        ],
      },
      {
        id: 1,
        date: "2020/02/2020",
        type: "Venta",
        weight: 30,
        total: 8.3,
        status: "Aceptado",
        detail: [
          {
            type: "carton",
            weight: "20",
            price: "122.2",
            total: "232.0",
          },
        ],
      },
    ];
    this.saleService.readSaleHistory().subscribe(
      (response: any) => {
        this.loading = false;
        this.sales = response;
      },
      (error: any) => {
        this.loading = false;
      }
    );
  }

  ngOnInit() {
    this.reset();
    this.getSales();
  }

  showDetail(saleId): void {
    this.isVisible = true;
    this.saleService.readDetailSale(saleId).subscribe(
      (response: any) => {
        this.materials = response;
      },
      (error: any) => {
        this.loading = false;
      }
    );
  }

  hideModal(): void {
    this.isVisible = false;
    this.materials = [];
  }
}
