import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesHistoryViewComponent } from './sales-history-view.component';

describe('SalesHistoryViewComponent', () => {
  let component: SalesHistoryViewComponent;
  let fixture: ComponentFixture<SalesHistoryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesHistoryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesHistoryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
