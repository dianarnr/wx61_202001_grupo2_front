import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewsRoutingModule } from './views-routing.module';
import { SalesHistoryViewComponent } from './sales-history-view/sales-history-view.component';
import { SharedModule as AdminSharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    SalesHistoryViewComponent
  ],
  imports: [
    CommonModule,
    ViewsRoutingModule,
    AdminSharedModule
  ]
})
export class ViewsModule { }
