import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewsModule as SalesHistoryViewsModule } from './views/views.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SalesHistoryViewsModule
  ]
})
export class SalesHistoryModule { }
