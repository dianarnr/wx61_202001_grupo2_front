import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
  public typeUser: any;
  public isVisibleModal = false;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.typeUser = JSON.parse(localStorage.getItem("userLogged")).type_user;
  }

  isActive(route: string) {
    var url = window.location.href;
    return url.indexOf(route) > -1;
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl("/auth/login");
  }
}
