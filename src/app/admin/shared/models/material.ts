export class Material {
	id: number;
	categoryId: string;
	name: string;
	description: string;
	previewImage: string;
	weight: string;
	price: string;
}
